package main

import (
	"os"

	"gitlab.com/nunet/firecracker-images/image-creator/rootfs"
)

func main() {
	args := os.Args[1:]

	containerImage := args[0]
	mountDirectory := "/tmp/rootfs"
	outputFolder := "./output"
	imageFile := outputFolder + "/" + "image.ext4"
	size := 5

	rootfs.PrepareOutputFolder(outputFolder)
	rootfs.CreateEmptyImage(size, imageFile)
	rootfs.MountImage(mountDirectory, imageFile)
	rootfs.CreateRootFsFromContainer(containerImage, mountDirectory)
	rootfs.UnMountImage(mountDirectory)
	rootfs.CheckImageFilesystem(imageFile)

}
