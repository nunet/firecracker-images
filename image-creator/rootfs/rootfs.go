package rootfs

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

//Creates the output folder to where the rootfs image will be stored
func PrepareOutputFolder(outputFolder string) {
	if err := os.Mkdir(outputFolder, 0750); err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}
}

// Creates an empty image of provided size in Gigabytes which will become the rootfs image.
func CreateEmptyImage(size int, imageFile string) {
	s := strconv.Itoa(size) + "G"
	fmt.Println("Creating rootfs image")
	truncate := exec.Command("truncate", "-s", s, imageFile)
	if err := truncate.Run(); err != nil {
		log.Fatal(err)
	}
	mkimg := exec.Command("mkfs.ext4", imageFile)
	if err := mkimg.Run(); err != nil {
		log.Fatal(err)
	}

}

//Mounts the rootfs image to a temp directory
func MountImage(mountDirectory string, imageFile string) {
	err := os.Mkdir(mountDirectory, 0750)
	if err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

	fmt.Printf("Mounting rootfs image to %s \n", mountDirectory)

	mount := exec.Command("mount", imageFile, mountDirectory)
	stderr, _ := mount.StderrPipe()
	err = mount.Start()

	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(stderr)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

}

// Unmounts the rootfs image
func UnMountImage(mountDirectory string) {
	unmount := exec.Command("umount", mountDirectory)
	if err := unmount.Run(); err != nil {
		log.Fatal(err)
	}
}

// Extract rootfs from Docker container
func CreateRootFsFromContainer(containerImage string, mountDirectory string) {
	fmt.Printf("Building rootfs from docker image: %s\n", containerImage)
	containerId, _ := exec.Command("docker", "create", containerImage).Output()
	cId := strings.Replace(string(containerId), "\n", "", -1)
	command := "sudo docker export " + string(cId) + " > rootfs.tar"
	export := exec.Command("bash", "-c", command)

	stderr, _ := export.StderrPipe()
	if err := export.Start(); err != nil {
		log.Fatal(err)
		fmt.Println("error")
	}
	scanner := bufio.NewScanner(stderr)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	export.Wait()

	extract := exec.Command("tar", "-C", mountDirectory, "-xf", "rootfs.tar")
	stderr2, _ := extract.StderrPipe()
	if err := extract.Start(); err != nil {
		log.Fatal(err)
	}
	scanner1 := bufio.NewScanner(stderr2)
	for scanner1.Scan() {
		fmt.Println(scanner1.Text())
	}

}

//Execute a script on rootFs

// Check rootfs image filesystem
func CheckImageFilesystem(imageFile string) {
	check := exec.Command("e2fsck", "-y", "-f", imageFile)
	if err := check.Run(); err != nil {
		log.Fatal(err)
	}
}
